#include <stdio.h>

int main(int argc, char *argv[])
{
	FILE *input;
	char line[100];
	if (argc != 2)
	{
		printf("Usage: absoloader <filename>\n");
		return -1;
	}
	input = fopen(argv[1], "r");
	if ( ! input)
	{
		printf("ERROR: Specified file not found.\n");
		return -1;
	}
	while ( ! feof(input))
	{
		char rec_type, prog_name[20];
		int start_loc, rec_size, rec_data;
		fscanf(input, "%s", line);
		rec_type = line[0];
		if (rec_type == 'H')
		{
			sscanf(line, "%*c%*c%[^^]", prog_name);
			printf("%s:\n", prog_name);
		}
		else if (rec_type == 'T')
		{
			int i, flag;
			char format_str[30];
			sscanf(line, "%*c%*c%x", &start_loc);
			flag = sscanf(line, "%*c%*c%*x%*c%*x%*c%2x", &rec_data);
			printf("%X\t%02X\n", start_loc, rec_data);
			start_loc++;
			for (i = 2; flag != EOF; i+=2, start_loc++)
			{
				sprintf(format_str, "%%*c%%*c%%*x%%*c%%*x%%*c%%*%dx%%2x", i);
				flag = sscanf(line, format_str, &rec_data);
				if (flag != EOF)
					printf("%X\t%02X\n", start_loc, rec_data);
			}
		}
	}
	return 0;
}
