#include <stdio.h>
#include <string.h>

typedef struct
{
	char name[5];
} sym;

typedef struct
{
	sym lhs;		// Left Hand Side and
	sym rhs[20];	// Right Hand Side, for lack of better/standard terminology
	int rhs_sym_count;
} prod;

prod G[100];
sym terminals[20];
sym nonterminals[20];
sym operators[5];
sym input[20];
int no_of_productions, no_of_terminals = 0, no_of_nonterminals = 0, no_of_operators = 5;
int no_of_input_symbols;

void print_sym(sym s);

int find_matching_prod(sym* input, int no_of_input_symbols)
{
	int i;
	for (i = 0; i < no_of_productions; i++)
	{
		int j;
		for (j = 0; G[i].rhs_sym_count == no_of_input_symbols && j < G[i].rhs_sym_count; j++)
		{
			if (strcmp(G[i].rhs[j].name, input[j].name) != 0)
				break;
		}
		if (j == G[i].rhs_sym_count)
			return i;
	}
	return -1;
}

int is_terminal(sym s)
{
	int i;
	for (i = 0; i < no_of_terminals; i++)
		if (strcmp(terminals[i].name, s.name) == 0)
			return 1;
	return 0;
}

int is_nonterminal(sym s)
{
	int i;
	for (i = 0; i < no_of_nonterminals; i++)
		if (strcmp(nonterminals[i].name, s.name) == 0)
			return 1;
	return 0;
}

int is_operator(sym s)
{
	int i;
	for (i = 0; i < no_of_operators; i++)
		if (strcmp(operators[i].name, s.name) == 0)
			return 1;
	return 0;
}

int precedence_of(sym s)
{
	switch (s.name[0])
	{
		case '^':
			return 2;
		case '*':
		case '/':
			return 1;
		case '+':
		case '-':
			return 0;
	}
}

void print_sym(sym s)
{
	printf("%s ", s.name);
}

void print_prod(prod p)
{
	int i;
	print_sym(p.lhs);
	printf("-> ");
	for (i = 0; i < p.rhs_sym_count; i++)
		print_sym(p.rhs[i]);
	printf("\n");
}

void print_grammar()
{
	int i;
	printf("Grammar: \n");
	for (i = 0; i < no_of_productions; i++)
	{
		printf("%d: ", i);
		print_prod(G[i]);
	}
}

void print_terminals()
{
	int i;
	printf("Terminals: ");
	for (i = 0; i < no_of_terminals; i++)
		print_sym(terminals[i]);
	printf("\n");
}

void print_nonterminals()
{
	int i;
	printf("Non-terminals: ");
	for (i = 0; i < no_of_nonterminals; i++)
		print_sym(nonterminals[i]);
	printf("\n");
}

void print_input()
{
	int i;
	for (i = 0; i < no_of_input_symbols; i++)
		print_sym(input[i]);
	printf("\n");
}

void read_grammar_from_file(char* filename)
{
	int i, j;
	FILE *file = fopen(filename, "r");
	if (feof(file))
		return;
	// Reads the file
	for (i = 0; i < 100 && !feof(file); i++)
	{
		int rhs_sym_count;			// Index for symbols on RHS of production
		int end_of_prod_flag = 0;
		int j;
		if (!fscanf(file, "%s -> ", G[i].lhs.name))
		{
			if (i == 0)
				return;
			else
				return;
		}
		if (!is_nonterminal(G[i].lhs))
			nonterminals[no_of_nonterminals++] = G[i].lhs;
		if (feof(file))
			break;
		for (rhs_sym_count = 0; !feof(file);)
		{
			int rhs_sym_char_count;		// Index for symbol names (read char by char)
			for (rhs_sym_char_count = 0; ;)
			{
				char c;
				fscanf(file, "%c", &c);
				if (c == ' ' || c == '\n')
				{
					rhs_sym_count++;
					rhs_sym_char_count = 0;
					if (c == '\n')
					{
						end_of_prod_flag = 1;
						break;
					}
					continue;
				}
				G[i].rhs[rhs_sym_count].name[rhs_sym_char_count++] = c;
			}
			if (end_of_prod_flag)
				break;
		}
		G[i].rhs_sym_count = rhs_sym_count;
		if (end_of_prod_flag)
			continue;
	}
	no_of_productions = i;
	for (i = 0; i < no_of_productions; i++)
		for (j = 0; j < G[i].rhs_sym_count; j++)
			if (!is_terminal(G[i].rhs[j]) && !is_nonterminal(G[i].rhs[j]))
				terminals[no_of_terminals++] = G[i].rhs[j];
}

int main(int argc, char* argv[])
{
	int i;
	char c = ' ';
	strcpy(operators[0].name, "+");
	strcpy(operators[1].name, "-");
	strcpy(operators[2].name, "*");
	strcpy(operators[3].name, "/");
	strcpy(operators[4].name, "^");
	read_grammar_from_file(argv[1]);
	printf("\n");
	print_grammar();
	printf("\n");
	print_terminals();
	print_nonterminals();
	printf("\n");
	printf("Input: ");
	for (i = 0; c != '\n'; i++)
		scanf("%s%c", input[i].name, &c);
	no_of_input_symbols = i;
	print_input();
	for (i = 0; i < no_of_input_symbols; i++)
	{
		if (is_terminal(input[i]))
		{
			int prod_index;
			prod_index = find_matching_prod(&(input[i]), 1);
			if (prod_index > 0)
				input[i] = G[prod_index].lhs;
		}
	}
	print_input();
	while (no_of_input_symbols > 1)
	{
		int prod_index, highest_op_index = -1;
		for (i = 0; i < no_of_input_symbols; i++)
			if (is_operator(input[i]))
				if (highest_op_index == -1 || precedence_of(input[i]) >= precedence_of(input[highest_op_index]))
					highest_op_index = i;
		prod_index = find_matching_prod(&(input[highest_op_index-1]), 3);
		if (prod_index >= 0)
		{
			int j;
			input[highest_op_index-1] = G[prod_index].lhs;
			for (j = highest_op_index; j < no_of_input_symbols; j++)
				input[j] = input[j+2];
			no_of_input_symbols -= 2;
		}
		print_input();
	}
}