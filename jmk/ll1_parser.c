#include <stdio.h>
#include <string.h>

typedef struct
{
	char name[5];
} sym;

typedef struct
{
	sym lhs;		// Left Hand Side and
	sym rhs[20];	// Right Hand Side, for lack of better/standard terminology
	int rhs_sym_count;
} prod;

typedef struct
{
	sym nonterminal;
	sym terminal;
	int prod_index;
} table_rec;

typedef struct
{
	sym eval[20];
	sym input[20];
} stack_struct;

stack_struct stack;
table_rec table[100];
prod G[100];
sym terminals[20];
sym nonterminals[20];
int no_of_productions, no_of_terminals = 0, no_of_nonterminals = 0, no_of_table_recs = 0;
int eval_top;
int input_top;

void print_sym(sym s)
{
	printf("%s ", s.name);
}

void print_prod(prod p)
{
	int i;
	print_sym(p.lhs);
	printf("-> ");
	for (i = 0; strcmp(p.rhs[i].name, "END") != 0; i++)
		print_sym(p.rhs[i]);
	printf("\n");
}

void print_grammar()
{
	int i;
	printf("Grammar: \n");
	for (i = 0; i < no_of_productions; i++)
	{
		printf("%d: ", i);
		print_prod(G[i]);
	}
}

void print_terminals()
{
	int i;
	printf("Terminals: ");
	for (i = 0; i < no_of_terminals; i++)
		print_sym(terminals[i]);
	printf("\n");
}

void print_nonterminals()
{
	int i;
	printf("Non-terminals: ");
	for (i = 0; i < no_of_nonterminals; i++)
		print_sym(nonterminals[i]);
	printf("\n");
}

void print_table()
{
	int i;
	printf("Table:\n\t|");
	for (i = 0; i < no_of_terminals; i++)
		printf("\t%s", terminals[i].name);
	printf("\n");
	for (i = 0; i < (no_of_terminals+2)*8; i++)
		printf("-");
	printf("\n");
	for (i = 0; i < no_of_table_recs;)
	{
		int count;
		printf("%s\t|", table[i].nonterminal.name);
		for (count = 0; count < no_of_terminals; count++)
		{
			if (table[i].prod_index == -1)
				printf("\t-");
			else
				printf("\t%d", table[i].prod_index);
			i++;
		}
		printf("\n");
	}
}

void print_stack()
{
	int i;
	printf("\n");
	for (i = 0; i <= input_top; i++)
		print_sym(stack.input[i]);
	printf("| ");
	for (i = 0; i <= eval_top; i++)
		print_sym(stack.eval[i]);
}

int is_terminal(sym s)
{
	int i;
	for (i = 0; i < no_of_terminals; i++)
		if (strcmp(terminals[i].name, s.name) == 0)
			return 1;
	return 0;
}

void read_grammar_from_file(char* filename)
{
	int i;
	FILE *file = fopen(filename, "r");
	if (feof(file))
		return;
	// Reads the file
	for (i = 0; i < 100 && !feof(file); i++)
	{
		int rhs_sym_count;			// Index for symbols on RHS of production
		int end_of_prod_flag = 0;
		int j;
		if (!fscanf(file, "%s -> ", G[i].lhs.name))
		{
			if (i == 0)
				return;
			else
				return;
		}
		if (feof(file))
			break;
		for (rhs_sym_count = 0; !feof(file);)
		{
			int rhs_sym_char_count;		// Index for symbol names (read char by char)
			for (rhs_sym_char_count = 0; ;)
			{
				char c;
				fscanf(file, "%c", &c);
				if (c == ' ' || c == '\n')
				{
					rhs_sym_count++;
					rhs_sym_char_count = 0;
					if (c == '\n')
					{
						end_of_prod_flag = 1;
						break;
					}
					continue;
				}
				G[i].rhs[rhs_sym_count].name[rhs_sym_char_count++] = c;
			}
			if (end_of_prod_flag)
				break;
		}
		strcpy(G[i].rhs[rhs_sym_count].name, "END");
		G[i].rhs_sym_count = rhs_sym_count;
		if (end_of_prod_flag)
			continue;
	}
	no_of_productions = i;
}

void read_table_from_file(char* filename)
{
	int i;
	sym nt, t;
	FILE *file = fopen(filename, "r");
	if (feof(file))
		return;
	do
	{
		fscanf(file, "%s | ", nt.name);
		if (strcmp(nt.name, "#") != 0)
		{
			nonterminals[no_of_nonterminals++] = nt;
			table[no_of_table_recs].nonterminal = nt;
			for (i = 0; i < no_of_terminals; i++, no_of_table_recs++)
			{
				table[no_of_table_recs].nonterminal = nt;
				table[no_of_table_recs].terminal = terminals[i];
				fscanf(file, "%d ", &(table[no_of_table_recs].prod_index));
			}
		}
		else
		{
			do
			{
				fscanf(file, "%s ", t.name);
				terminals[no_of_terminals++] = t;
			} while (strcmp(t.name, "$") != 0);
		}
	} while (!feof(file));
}

int parse()
{
	int i, j, input_len;
	char sym_name[5], c;
	sym tmp[20];
	input_top = 0;
	eval_top = 0;
	// Prepare input stack
	printf("Input string: ");
	while (scanf("%s%c", sym_name, &c))
	{
		strcpy(tmp[input_top].name, sym_name);
		input_top++;
		if (c == '\n')
			break;
	}
	strcpy(tmp[input_top].name, "$");
	//Reverses input stack into the right order
	for (i = input_top, j = 0; i >= 0; i--, j++)
		stack.input[j] = tmp[i];
	// Prepare eval stack
	strcpy(stack.eval[eval_top++].name, "$");
	stack.eval[eval_top] = nonterminals[0];
	printf("\nParsing:");
	while (1)
	{
		print_stack();
		// Checks for end of stacks
		if (strcmp("$", stack.eval[eval_top].name) == 0)
		{
			if (strcmp("$", stack.input[input_top].name) == 0)
			{
				printf("| accept $");
				return 1;
			}
			else
				return 0;
		}
		// Checks if both tops are terminals
		else if (is_terminal(stack.eval[eval_top]))
		{
			if (strcmp(stack.input[input_top].name, stack.eval[eval_top].name) == 0)
			{
				printf("| accept %s", stack.input[input_top].name);
				input_top--;
				eval_top--;
			}
			else
				return 0;
		}
		// Matches production
		else
		{
			int prod_index = -1;
			for (i = 0; i < no_of_table_recs; i++)
			{
				if (strcmp(table[i].nonterminal.name, stack.eval[eval_top].name) == 0)
					if (strcmp(table[i].terminal.name, stack.input[input_top].name) == 0)
					{
						prod_index = table[i].prod_index;
						break;
					}
			}
			if (i == no_of_table_recs)
				return 0;
			for (i = G[prod_index].rhs_sym_count-1; i >= 0; i--)
			{
				// If nullable, pops without pushing anything
				if (strcmp("ε", G[prod_index].rhs[i].name) == 0)
					continue;
				// Else, pushes the RHS of matching production
				stack.eval[eval_top++] = G[prod_index].rhs[i];
			}
			printf("| apply %d", prod_index);
			eval_top--;
		}
	}
}

int main(int argc, char* argv[])
{
	int success;
	read_grammar_from_file(argv[1]);
	read_table_from_file(argv[2]);
	printf("\n");
	print_grammar();
	printf("\n");
	print_terminals();
	print_nonterminals();
	printf("\n");
	print_table();
	printf("\n");
	success = parse();
	printf("\n\n");
	if (success)
		printf("SUCCESS: Input string is valid.\n");
	else
		printf("ERROR: Invalid input string.\n");
	return 0;
}