%{
	#include<stdio.h>
%}
%token NUM 
%left "*" "/"
%left "+" "-"

%%

S : E '\n' {printf("Answer: %d",$1);}
    ;
E : E '+' E {$$ = $1 + $3;}
  |E '-' E {$$ = $1 - $3;}
  |E '*' E {$$ = $1 * $3;}
  |E '/' E {$$ = $1 / $3;}
  | NUM {$$ = $1;}
  ;
%%

main()
{
	
	printf("\nCalculation: ");
	yyparse();
}

int yyerror()
{
}
