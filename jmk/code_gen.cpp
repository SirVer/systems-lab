// For fuck's sake, why did you even try?

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

class Symbol
{
public:
	string name;
	Symbol(){};
	Symbol(string);
	~Symbol(){};
	bool isOperator()
	{
		if (name == "+" || name == "-" || name == "*" || name == "/" || name == "^")
			return true;
		else
			return false;
	}
	bool isSubexpression()
	{
		if (name[0] == '#')
			return true;
		else
			return false;
	}
	int precedence()
	{
		if (isOperator())
			return -1;
		if (name == "+" || name == "-")
			return 0;
		else if (name == "*" || name == "/")
			return 1;
		else if (name == "^")
			return 2;
		else
			return -1;
	}
};

Symbol::Symbol(string s)
{
	name = s;
}

Symbol subexpressions[10][3];
int no_of_subexp = 0;

Symbol addSubexpression(Symbol* exp, int start_index)
{
	string subexp_name;
	subexpressions[no_of_subexp][0] = exp[start_index];
	subexpressions[no_of_subexp][1] = exp[start_index+1];
	subexpressions[no_of_subexp][2] = exp[start_index+2];
	subexp_name = "#" + to_string(no_of_subexp);
	no_of_subexp++;
	return subexp_name;
}

void generateIntermediateCode(Symbol* exp, int no_of_symbols)
{
	int op_index = -1;	// Index of highest precedence operator in exp
	int complete_flag = 0;
	while (!complete_flag)
	{
		for (int i = 0; i < no_of_symbols; i++)
			if (exp[i].isOperator())
			{
				if (op_index == -1)
					op_index = i;
				else if (exp[i].precedence() > exp[op_index].precedence())
					op_index = i;
			}
		if (op_index <= 0 || op_index >= no_of_symbols)
		{
			cout << "ERROR\n";
			return;
		}
		if (!exp[op_index-1].isSubexpression() || !exp[op_index+1].isSubexpression())
		{
			Symbol tmp = addSubexpression(exp, op_index-1);
			for (int i = op_index-1; i < no_of_symbols; i++)
				exp[i] = exp[i+2];
			no_of_symbols -= 2;
			exp[op_index-1] = tmp;
		}
		for (int i = 0, complete_flag = 1; i < no_of_symbols; i++)
			if (!exp[i].isSubexpression())
				complete_flag = 0;
	}
}

void printExpression(Symbol *exp, int no_of_symbols)
{
	for (int i = 0; i < no_of_symbols; i++)
		cout<<exp[i].name;
	cout<<endl;
}

int main()
{
	Symbol exp[] = {Symbol("a"), Symbol("+"), Symbol("b")};
	generateIntermediateCode(exp, 3);
	printExpression(exp, 3);
}