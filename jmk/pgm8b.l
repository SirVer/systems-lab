%{
	#include<stdio.h>
	#include "y.tab.h"
	
%}
DIG [0-9]
dot [/.]
%%
{DIG} {yylval = atoi(yytext); return NUM;}
"+" {return *(yytext);}
"-" {return *(yytext);}
"*" {return *(yytext);}
"/" {return *(yytext);}
"\n" {return yytext[0];}
{dot} return yytext[0];
%%

int yywrap()
{
}
