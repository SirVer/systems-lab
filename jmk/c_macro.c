#include <stdio.h>
#include <string.h>
#include <malloc.h>

typedef struct
{
	char* name;
	char* def;
} macro_rec;

int no_of_macros = 0;
macro_rec deftab[50];

void define(char* r_line)
{
	deftab[no_of_macros].name = malloc(200*sizeof(char));
	deftab[no_of_macros].def = malloc(200*sizeof(char));
	sscanf(r_line, "%*s %s %s", deftab[no_of_macros].name, deftab[no_of_macros].def);
	no_of_macros++;
}

int check_for_macro(char* r_line)
{
	int i;
	for (i = 0; i < no_of_macros; i++)
		if (strstr(r_line, deftab[i].name))
			return i;
	return -1;
}

void sort_macro_list()
{
	int i, j;
	for (i = 0; i < no_of_macros && no_of_macros > 1; i++)
		for (j = i; j < no_of_macros-1; j++)
			if (strlen(deftab[j].name) < strlen(deftab[j+1].name))
			{
				macro_rec tmp;
				tmp = deftab[j];
				deftab[j] = deftab[j+1];
				deftab[j+1] = tmp;
			}
}

int main(int argc, char* argv[])
{
	FILE *input, *output;
	char *r_line, *w_line;
	int which_macro, lines = 0;
	if (argc != 3)
	{
		printf("Usage: c_macro <input_file> <output_file>\n");
		return -1;
	}
	input = fopen(argv[1], "r");
	output = fopen(argv[2], "w");
	if (feof(input))
	{
		printf("ERROR: File not found\n");
		return -2;
	}
	r_line = malloc(200*sizeof(char));
	while ( ! feof(input))
	{
		fscanf(input, "%[^\n]\n", r_line);
		if (strstr(r_line, "#define"))
		{
			define(r_line);
			fprintf(output, "%s\n", r_line);
			sort_macro_list();
		}
		else if ((which_macro = check_for_macro(r_line)) >= 0)
		{
			int offset;
			char *write_to;
			char *read_from = strstr(r_line, deftab[which_macro].name);
			w_line = malloc(strlen(r_line) + strlen(deftab[which_macro].name) + 1);
			strcpy(w_line, r_line);
			write_to = strstr(w_line, deftab[which_macro].name);
			offset = sprintf(write_to, "%s", deftab[which_macro].def);
			sprintf(write_to + offset, "%s", read_from + strlen(deftab[which_macro].name));
			fprintf(output, "%s\n", w_line);
		}
		else
			fprintf(output, "%s\n", r_line);
	}
	free(r_line);
	free(w_line);
	return 0;
}