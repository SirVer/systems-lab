#include <stdio.h>

int main()
{
	int x, data;
	char str[100], c;
	scanf("%s", str);
	sscanf(str, "%c%*c%*x%*c%*x%*c%0x%2x", &c, &x, &data);
	printf("%c %x %x\n", c, x, data);
	return 0;
}