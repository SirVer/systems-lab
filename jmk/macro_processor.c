//todo: GETLINE, DEFINE, EXPAND

#include <stdio.h>
#include <string.h>

typedef struct deftab_rec
{
	char* prototype;
	char* def[100];
} deftab_rec;

typedef struct namtab_rec
{
	char* macro_name;
	deftab_rec* start;
	deftab_rec* end;
} namtab_rec;

typedef struct argtab_rec
{
	char* macro_name;
	char* args[10];
} argtab_rec;

deftab_rec DEFTAB[100];
namtab_rec NAMTAB[100];
argtab_rec ARGTAB[100];

int EXPANDING = 0, no_of_macros = 0;
char* line, OPCODE;
FILE* src, dest;

void readline()
{
	fscanf(src, "%s", line);
	if strstr(line, "MACRO")
		sprintf(OPCODE, "MACRO");
	else
		sscanf(line, "%[^ ]", OPCODE);
}

void writeline(char* line)
{
	fprintf(dest, "%s\n", line);
}

void GETLINE()
{
	if (EXPANDING)
	{
	}
	else
		readline();
}

void DEFINE()
{
	sscanf(line, "%[^ ]", NAMTAB[no_of_macros].macro_name);
	strcpy(DEFTAB[no_of_macros].prototype, line);
	no_of_macros++;
}

void PROCESSLINE()
{
	int i;
	for (i = 0; i < no_of_macros; i++)
	{
		if (strcmp(OPCODE, NAMTAB[i].macro_name) == 0)
			EXPAND();
		else if (strcmp(OPCODE, "MACRO") == 0)
			DEFINE();
		else writeline(line);
	}
}

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		printf("USAGE: macro_processor <filename>\n");
		return -1;
	}
	src = fopen(argv[1], "r");
	if (!src)
	{
		printf("ERROR: Failed to open file, does it exist?\n");
		return -2;
	}
	dest = fopen("macro_output", "w")
	readline();
	while (strcmp(OPCODE, "END") != 0)
	{
		GETLINE();
		PROCESSLINE();
	}
	fclose(src);
}