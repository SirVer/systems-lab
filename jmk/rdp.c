#include <stdio.h>
#include <ctype.h>
#include <string.h>

int c = 0, f = 0;
char expr[50];

void E();
void E1();
void T();
void T1();
void F();

void E()
{
	T();
	E1();
}

void E1()
{
	if (expr[c] == '+')
	{
		c++;
		T();
		E1();
	}	
}

void T()
{
	F();
	T1();
}

void T1()
{
	if (expr[c] == '*')
	{
		c++;
		F();
		T1();
	}
}

void F()
{
	if (expr[c] == '(')
	{
		c++;
		E();
		if (expr[c] == ')')
			c++;
		else
			f = 1;
	}
	else if (isalpha(expr[c]))
		c++;
	else
		f = 1;
}

void main()
{
	printf("\nExpression: ");
	scanf("%s", expr);
	E();
	if (strlen(expr) == c && f == 0)
		printf("Valid.\n\n");
	else
		printf("Invalid\n\n");
}