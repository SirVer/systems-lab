#include<stdio.h>

#include<string.h>
#include<stdlib.h>
struct quadraple
{
	int pos;
	char op;
	char arg1[5];
	char arg2[5];
	char result[5];
}quad[15];

int n=0;
void assignment(int);
void uminus(int );
void explore();
void codegen(char op[5],int);
char tuple[15][15];
void tree(char *res)
{
	for (int i = n-1; i >= 0; --i)
	{
		if(!strcmp(res,quad[i].result))
		{
			if (i==n-1)
			{
				printf("%s ",quad[i].result);
			}
			printf(" %s %s ",quad[i].arg1,quad[i].arg2);
			tree(quad[i].arg1);
			tree(quad[i].arg2);

		}
	}
}
int main(void)
{
	int nRetInd,i;
	char str[15];
	printf("Intermediate code (enter 'end' when done):\n");
	while (1)
	{
		scanf("%s",str);
		if (strcmp(str, "end") == 0)
			break;
		strcpy(tuple[n++],str);
	}
	
	explore();
	printf("\nTree: ");
	tree(quad[n-1].result);
	printf("\n\nQuadruple: \n");
	printf("pos\t opr\t arg1\t arg2\t result\n");
	printf("--------------------------------------------");
	for(i=0;i<n;i++)
		printf("\n%d\t|%c\t|%s\t|%s\t|%s",quad[i].pos,quad[i].op,quad[i].arg1,quad[i].arg2,quad[i].result);
	i=0;
	printf("\n\nAssembly:\n");
	while(i<n)
	{
		if(quad[i].op=='+')
			codegen("ADD",i);
		if(quad[i].op=='=')
			assignment(i);
		if(quad[i].op=='-')
				if(!strcmp(quad[i].arg2,"\0"))
					uminus(i);
				else
					codegen("SUB",i);
		if(quad[i].op=='*')
			codegen("MUL",i);
		if(quad[i].op=='/')
			codegen("DIV",i);
	i++;
	}
return 0;
}
void codegen(char op[5],int t)
{
	char str[25];
	printf("MOV %s,R0\n",quad[t].arg1);
	printf("%s R0,%s\n",op,quad[t].arg2);
	printf("MOV R0,%s\n",quad[t].result);
}
void assignment(int t)
{
	char str[25];
	printf("MOV %s,%s\n",quad[t].result,quad[t].arg1);
}
void uminus(int t)
{
	char str[25];
	printf("MOV R0,0\n");
	printf("SUB %s,R0\n",quad[t].arg1);
	printf("MOV R0,%s\n",quad[t].result);
}

void explore()
{
	int i,j,t,t1,t2;
	for(i=0;i<n;i++)
	{
		quad[i].pos=i;
		for(j=0,t=0;j<strlen(tuple[i])&&tuple[i][j]!='=';j++)
		{
			quad[i].result[t++]=tuple[i][j];
		}
		t1=j;
		quad[i].result[t]='\0';
		if(tuple[i][j]=='=')
		{
			quad[i].op='=';
		}
		if(tuple[i][j+1]=='+'||tuple[i][j+1]=='-'||tuple[i][j+1]=='*'||tuple[i][j+1]=='/')
		{
			quad[i].op=tuple[i][j+1];
			t1=j+1;
		}
		for(j=t1+1,t=0;j<strlen(tuple[i])&&tuple[i][j]!='+'&&tuple[i][j]!='-'&&tuple[i][j]!='*'&&tuple[i][j]!='/';j++)
		{
			quad[i].arg1[t++]=tuple[i][j];
		}
		t2=j;
		quad[i].arg1[t]='\0';
		if(tuple[i][j]=='+'||tuple[i][j]=='-'||tuple[i][j]=='*'||tuple[i][j]=='/')
			{
				quad[i].op=tuple[i][j];
			}
		for(j=t2+1,t=0;j<strlen(tuple[i]);j++)
		{
			quad[i].arg2[t++]=tuple[i][j];
		}
		quad[i].arg2[t]='\0';
	}
}








