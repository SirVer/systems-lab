%{
	#include<stdio.h>

%}
l [a-zA-Z]
n [0-9]
o "+"|"-"|"*"|"/"|"%"|"="
k "if"|"if else"|"goto"|"while"|"do"|"int"|"scanf"|"printf"|"include"
h "stdio.h"|"stdlib.h"
i "main()"
%%
{l} {printf("\n%s is a Literal",yytext);}
{n} {printf("\n%s is a Number",yytext);}
{o} {printf("\n%s is an Operator",yytext);}
{k} {printf("\n%s is a Keyword",yytext);}
{h} {printf("\n%s is a Header file",yytext);}
{i}|{l}*{n}* {printf("\n%s is an Identifier",yytext);}
%%
void main()
{
	yyin=fopen("input_lex_c_parser","r");
	yylex();
	fclose(yyin); 
}
extern int yywrap(void)
{

}
