/*
- May need a relational operator list (OP_LIST) as well
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define ERROR -1
#define NO_OF_KEYWORDS 10

int TRANSTAB[6][6] = {{3, 1, 2, ERROR, ERROR},
					{ERROR, 1, 1, 1, ERROR},
					{ERROR, ERROR, 2, 4, ERROR},
					{5, ERROR, ERROR, ERROR, ERROR},
					{ERROR, ERROR, 4, ERROR, ERROR},
					{ERROR, ERROR, ERROR, ERROR, ERROR}};
char KWORD_LIST[100][100] = {"int", "float", "char", "main", "#include", "if", "else", "while", "printf", "getch"};

int next_state(int current_state, int input)
{
	int input_cat;	// input category (letter, digit, etc)
	if (input == '<' || input == '=' || input == '>' || input == '+' || input == '-' || input == '/' || input == '*')
		input_cat = 0;
	else if (isalpha(input) || input == '_' || input == '#')
		input_cat = 1;
	else if (isdigit(input))
		input_cat = 2;
	else if (input == '.')
		input_cat = 3;
	else
		input_cat = 4;
	return TRANSTAB[current_state][input_cat];
}

int main(int argc, char const *argv[])
{
	int current_state = 0, prev_state = 0, i;
	FILE *input_file;
	char word[100];
	char keywords[100][100], identifiers[100][100], operators[3][100], constants[100][100];
	int id_index = 0, kw_index = 0, op_index = 0, const_index = 0;
	if (argc < 2)
	{
		printf("Usage: lexa <filename>\n");
		return -1;
	}
	input_file = fopen(argv[1], "r");
	if (feof(input_file))
	{
		printf("ERROR: Could not open file, does it exist?\n");
		return -2;
	}
	while ( ! feof(input_file))
	{
		int i, j, trans_index = ERROR, flag;
		current_state = 0;
		fscanf(input_file, "%[^ ,;\n]", word);
		fgetc(input_file);
		//printf("%s$\n", word);
		for (i = 0; word[i] != '\0' && word[i] != ';' && current_state != ERROR; i++)
			current_state = next_state(current_state, word[i]);
		if (current_state == 1)
		{
			flag = -1; // Indicates identifier
			for (i = 0; i < NO_OF_KEYWORDS; i++)
				if (strcmp(word, KWORD_LIST[i]) == 0)
				{
					flag = 0; // Indicates keyword
					for (j = 0; j < kw_index; j++)
						if (strcmp(word, keywords[j]) == 0)
							flag = 1; // Indicates keyword already in list
					if (!flag)
						strcpy(keywords[kw_index++], word);
					break;
				}
			if (flag == -1)
			{
				for (j = 0; j < id_index; j++)
					if (strcmp(word, identifiers[j]) == 0)
						flag = -2; // Indicates identifier already in list
				if (flag == -1)
					strcpy(identifiers[id_index++], word);
			}
		}
		else if (current_state == 2 || current_state == 4)
			strcpy(constants[const_index++], word);
		else if (current_state == 3 || current_state == 5)
		{
			flag = 0; // Indicates operator
			for (j = 0; j < op_index; j++)
				if (strcmp(word, operators[j]) == 0)
					flag = 1; // Indicates operator already in list
			if (!flag)
				strcpy(operators[op_index++], word);
		}
		/*else
			printf("ERR: %d\n", current_state);*/
	}
	// Results
	printf("\nKeywords:\t");
	for (i = 0; i < kw_index; i++)
		printf("%s\t", keywords[i]);
	printf("\nIdentifiers:\t");
	for (i = 0; i < id_index; i++)
		printf("%s\t", identifiers[i]);
	printf("\nConstants:\t");
	for (i = 0; i < const_index; i++)
		printf("%s\t", constants[i]);
	printf("\nOperators:\t");
	for (i = 0; i < op_index; i++)
		printf("%s\t", operators[i]);
	printf("\n");
	return 0;
}